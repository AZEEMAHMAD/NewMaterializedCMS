@extends('layouts.master')
@section('content')
    <div id="basic-form" class="section">
        <div class="row">
            <div class="col s12">
                <nav class="light  cyan darken-2">
                    <div class="nav-wrapper">
                        <div class="col s12">
                            <span class="brand-logo">Edit Post #{{ $post->id }}</span>
                        </div>
                    </div>
                </nav>
            </div>
            <br/>
            <div class="col s6 m4 l6">
                <div class="card">
                    <div class="row">
                        <a href="{{ url('/posts') }}" title="Back" class="btn waves-effect waves-light teal"><i
                                    class="mdi-navigation-arrow-back"></i> Back</a>
                        <br/>
                        <br/>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/posts/' . $post->id) }}" accept-charset="UTF-8"
                              class="form-horizontal col s12" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('posts.form', ['formMode' => 'edit'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
