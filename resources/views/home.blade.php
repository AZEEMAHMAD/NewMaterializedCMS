@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="section">
                    <div id="roboto">
                        <h4 class="header">Welcome to DashBoard</h4>
                        <p class="caption">
                            The standard font Material Design uses is Roboto. We have included the font files with our
                            framework.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
