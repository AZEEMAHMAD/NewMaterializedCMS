@extends('layouts.app')
@section('content')
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form class="login-form" style="width:393px" method="post" action="{{url('unlock')}}">
            @csrf
            <div class="row">
                <div class="input-field col s12 center">
                    <img src="{{asset('images/avatar.jpg')}}" alt="" class="circle responsive-img valign profile-image-login">
                    <h4 class="header">{{Session::get('adminname')}}</h4>
                </div>
            </div>
            <input type="hidden" name="email" value="{{Session::get('adminemail')}}">
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password" type="password" name="password">
                    <label for="password">Password</label>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button type="submit" class="btn waves-effect waves-light col s12">
                        {{ __('Login') }}
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 m6 l6">
                    <p class="margin medium-small"><a href="{{url('/register')}}">Register Now!</a></p>
                </div>
                <div class="input-field col s6 m6 l6">
                    <p class="margin right-align medium-small"><a href="{{url('/password/reset')}}">Forgot password ?</a></p>
                </div>
            </div>

        </form>
    </div>
</div>
@endsection
