<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request){

        $this->validate($request, [
            'email' => 'required|email|exists:users',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::where('email',$request->email)->first();
        $user->password=Hash::make($request['password']);
        $user->save();
        DB::table('password_resets')->where('email', $user->email)->delete();
        $this->guard()->login($user);
        return redirect($this->redirectTo);

    }
}
