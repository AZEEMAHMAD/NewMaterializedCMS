<?php
Route::get('/', function () {
    return view('welcome');
});
Route::get('/lock','IndexController@lock')->name('lock');
Route::post('/unlock','IndexController@unlock')->name('unlock');

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::resource('posts', 'PostsController');
});



